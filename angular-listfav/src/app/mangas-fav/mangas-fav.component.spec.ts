import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MangasFavComponent } from './mangas-fav.component';

describe('MangasFavComponent', () => {
  let component: MangasFavComponent;
  let fixture: ComponentFixture<MangasFavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MangasFavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangasFavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
