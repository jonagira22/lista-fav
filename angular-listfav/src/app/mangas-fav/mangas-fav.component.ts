import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { MangasFav } from './../models/mangas-fav.model';

@Component({
  selector: 'app-mangas-fav',
  templateUrl: './mangas-fav.component.html',
  styleUrls: ['./mangas-fav.component.css']
})
export class MangasFavComponent implements OnInit {
	@Input() manga: MangasFav;
	@HostBinding('attr.class') cssClass= 'col-md-4';

  constructor() {}

  ngOnInit(): void {
  }

}
