export class MangasFav{
	nombre:string;
	url:string;
	sinopsis:string;

	constructor(n:string, u:string,	s:string){
		this.nombre=n;
		this.url=u;
		this.sinopsis=s;
	}
}