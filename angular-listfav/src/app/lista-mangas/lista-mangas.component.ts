import { Component, OnInit } from '@angular/core';
import { MangasFav } from './../models/mangas-fav.model'

@Component({
  selector: 'app-lista-mangas',
  templateUrl: './lista-mangas.component.html',
  styleUrls: ['./lista-mangas.component.css']
})
export class ListaMangasComponent implements OnInit {
	mangas:MangasFav[];
  constructor() {
  	this.mangas=[];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string,url:string,sinop:string):boolean{
  	this.mangas.push(new MangasFav(nombre,url,sinop));
  	return false;
  }

}
