import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MangasFavComponent } from './mangas-fav/mangas-fav.component';
import { ListaMangasComponent } from './lista-mangas/lista-mangas.component';

@NgModule({
  declarations: [
    AppComponent,
    MangasFavComponent,
    ListaMangasComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
